
#include "std_lib_facilities.h"
#include <AnimationWindow.h>

//------------------------------------------------------------------------------'


class Date{
	private:
		double seconds;
		int month;
		int day;
		int hour;
		int minute;
		string date;
	public:
		Date(double cseconds, int minutes, int hours, int days, int months){
			seconds = cseconds;
			minute = minutes;
			hour = hours;
			day = days;
			month = months;
			date = "";
		}

		double getSeconds(){
			return seconds;
		}

		void setSeconds(double newSeconds){
			seconds = newSeconds;
		}

		string getDate(){
			return date;
		}

		int getMonthLength(int inputMonth){
			if(inputMonth == 1 || inputMonth == 3 || inputMonth == 5 || inputMonth == 7 || inputMonth == 8 || inputMonth == 10 || inputMonth == 12){
				return 31;
			}
			else if(inputMonth == 2){
				return 28;
			}
			else{
				return 30;
			}
		}

		void updateDate(double numSeconds){
			seconds += numSeconds;
			int sec = static_cast<int>(numSeconds);
			int days = sec/86400;
			sec = sec%86400;
			int hours = sec/3600;
			sec = sec%3600;
			int minutes = sec/60;
			sec = sec%60;

			day += days;
			hour += hours;
			minute += minutes;


			if(minute > 59){
				hour += minute / 60;
				minute = minute % 60;
			}
			if(hour > 23){
				day += hour / 24;
				hour = hour % 24;
			}

			if(day > getMonthLength(month)){
				day -= getMonthLength(month);
				month++;
				if(month > 12){
					month = month % 12;
				}
			}

			double secDouble = static_cast<double>(sec);
			seconds -= secDouble;
			string zero = "";
			if(hour<10){
				zero = "0";
			}

			stringstream ss;
			ss << day << "." << month << "  " << zero << hour << ":" << minute;
			date = ss.str();
			

		}
};

class Planet{
	private:
		double x;
		double y;
		double dx;
		double dy;
		string name;
		double mass;
		double radius;
		bool star;
		Color color;
	
	public:

		Planet(double xCoordinate, double yCoordinate, double xVelocity, double yVelocity, string planetName, double planetMass, double planetRadius, bool boolStar, Color planetColor){
			x = xCoordinate;
			y = yCoordinate;
			dx = xVelocity;
			dy = yVelocity;
			name = planetName;
			mass = planetMass;
			radius = planetRadius;
			star = boolStar;
			color = planetColor;
		}
		

		void setX(double newx){
			x = newx;
		}

		double getX(){
			return x;
		}

		int getVisualX(){
			return static_cast<int>(this->getX()*90);
		}

		void setY(double newy){
			y = newy;
		}

		double getY(){
			return y;
		}

		int getVisualY(){
			return static_cast<int>(this->getY()*90);
		}

		void setDX(double newdx){
			dx = newdx;
		}

		double getDX(){
			return dx;
		}

		void setDY(double newdy){
			dy = newdy;
		}

		double getDY(){
			return dy;
		}

		void setName(string planetname){
			name = planetname;
		}

		string getName(){
			return name;
		}

		double getMass(){
			return mass;
		}
		
		double getRadius(){
			return radius;
		}

		bool isStar(){
			return star;
		}

		Color getColor(){
			return color;
		}

		void accelerate(vector<Planet> planetlist, double step){
			for(long unsigned int i = 0; i<planetlist.size(); i++){
				Planet planet = planetlist[i];
				if(this->getName().compare(planet.getName())==0){
					continue;
				}
				if(this->isStar()){
					continue;
				}
				double diffx = this->getX()-planet.getX();
				double diffy = this->getY()-planet.getY();
				this->setDX((this->getDX()-(step*(planet.getMass()*diffx/(pow(diffx*diffx+diffy*diffy, 3.0/2.0))))));
				this->setDY((this->getDY()-(step*(planet.getMass()*diffy/(pow(diffx*diffx+diffy*diffy, 3.0/2.0))))));
				
				
			}
			this->setX(this->getX()+step*(this->getDX()));
			this->setY(this->getY()+step*(this->getDY()));
			
		}
};



int main()
{
	// 0.004559603 is distance from Europa to Jupiter
	cout << "--------------" << endl;
	double solarmass = 0.000000000000039625; //Increased last time
	Planet sun = Planet(0,0,0,0,"Sun",solarmass,10, true, Color::yellow);
	Planet jupiter = Planet(4.9513405276,0,0,0.00000008736755369,"Jupiter",solarmass/1047.0,3, false, Color::green);
	Planet earth = Planet(1,0,0,0,"Earth", solarmass/333000.0,2, false, Color::blue);
	Planet europa = Planet(4.955825217,0,0,0.00000008736755369*1.1, "Europa", solarmass*0.000000024, 2, false, Color::brown);
	vector<Planet> planetlist = {sun,earth, jupiter};

	double N = 1000.0;
	double L = 31556926.0;
	double step = L/N;

	Date date = Date(0.0, 29, 23, 11, 11);
	

	AnimationWindow window;
	int middlex = window.width()/2;
	int middley = window.height()/2;

	for(double i = 0; i<N; i++){
		for(long unsigned int j = 1; j<planetlist.size(); j++){
			planetlist[j].accelerate(planetlist, step);
			//double x = planetlist[j].getX()*planetlist[j].getX();
			//double y = planetlist[j].getY()*planetlist[j].getY();
			//cout << planetlist[j].getName() << " x=" << planetlist[j].getX() << " y=" << planetlist[j].getY() << endl;
			//cout << planetlist[j].getName() << " distance = " << pow(x+y, 1.0/2.0) << endl;
			
		}

		date.updateDate(step);
		
		for(long unsigned int k = 0; k<planetlist.size(); k++){
			window.draw_circle({middlex + planetlist.at(k).getVisualX(), middley + planetlist.at(k).getVisualY()}, planetlist.at(k).getRadius(),planetlist.at(k).getColor());
			window.draw_text({0,50}, date.getDate());
		}
		
		
		window.next_frame();
	}

	
	return 0;
}

//------------------------------------------------------------------------------
